package hellopackage;

import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter an integer: ");
        int userInput = scanner.nextInt();
        scanner.close();

        Random rand = new Random();
        int randomNumber = rand.nextInt(10);

        System.out.println("User input: " + userInput);
        System.out.println("Random number: " + randomNumber);

        int doubledValue = Utilities.doubleMe(userInput);
        System.out.println("Doubled value: " + doubledValue);
    }
}